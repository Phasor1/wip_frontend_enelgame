# Test frontend Enel Game Online
## Structure
2 parts:

 - static page (index.html)
 - Vue project (test_vue_pixi)

## testing
To test the static part, create a web server on main folder with port 8080

 - the address without the iframe: ```http://localhost:8080```
 - the address with the iframe: ```http://localhost:8080/mario.html```

on file ```mario.html``` be sure that the field ```src``` of the iframe is ```http://localhost:8080```

To test the Vue part, go to test_vue_pixi folder and:

```npm i```

and

```npm run serve```
