'use strict';
//Create a Pixi Application
let app = new PIXI.Application({
    transparent: true,
    width: window.innerWidth,
    height: window.innerHeight
});
//Add the canvas that Pixi automatically created for you to the HTML document
app.view.width = window.innerWidth;
app.view.height = window.innerHeight;

document.body.appendChild(app.view);
let centrali = ['centrale_02_eolica2', 'centrale_00_solare2', 'centrale_03_geotermica2', 'centrale_06_gas2']
let centSprites = [];
let centraliUrl = window.location.href + '/assets/';
let cenTextures = [];
let numCentrali = 10;
// app.loader
//     .add('spritesheet', 'http://localhost:8082/centrale_00_solare2/centrale_00_solare2-0.png')
//     .load(onAssetsLoaded);

onAssetsLoaded();
function onAssetsLoaded() {
    console.log('inside')
    // create an array to store the textures
    let i;
    let centraleOffx = window.innerWidth / numCentrali;
    let centraleOffy = window.innerHeight / centrali.length;
    centrali.forEach((c, i) => {
        let cenTextures = [];
        for (let y = 0; y < 26; y++) {
            const texture = PIXI.Texture.from(centraliUrl + c + '/' + c + '-' + y + '.png');
            cenTextures.push(texture);
        }
        for (let y = 25; y >=0; y--) {
            const texture = PIXI.Texture.from(centraliUrl + c + '/' + c + '-' + y + '.png');
            cenTextures.push(texture);
        }
        for (let z = 0; z < 10; z++) {
        // create an cen AnimatedSprite
            var cen = new PIXI.AnimatedSprite(cenTextures);

            cen.x = centraleOffx*z;
            cen.y = centraleOffy*i
            // cen.width = 20;
            // cen.height = 20;
            cen.scale.set(0.2);
            cen.gotoAndPlay(0);
            centSprites.push(cen);
        }
    })
    centSprites.forEach(c =>{
        c.animationSpeed = 0.5;
        app.stage.addChild(c);
    })
    // start animating
    app.start();
}
console.log('some data', window.location, window.innerWidth, window.innerHeight)
// setInterval(() => {
//     centSprites.forEach(s => {
//         s.x += 3;
//         s.y += 3;
//     })
// }, 1000)