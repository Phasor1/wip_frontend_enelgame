import cv2
import time
import numpy as np
from os import walk, mkdir
first = True
num_files = 0
files_counter = 0
main_folder = './mainfolder'
for path, folders, files in walk(main_folder):	
	for file in files:
		if file.find('.mov') > -1 and first:
			num_files += 1
print(num_files)
for path, folders, files in walk(main_folder):	
	# vidcap = cv2.VideoCapture('/path/to/video')
	# success,image = vidcap.read()
	for file in files:
		if file.find('.mov') > -1 and first:
			print(path, file)
			current_path = main_folder + '/' + file.replace('.mov', '')
			mkdir(current_path)
			vidcap = cv2.VideoCapture(path + '/' + file)
			success,image = vidcap.read()
			print(image)
			counter = 0
			while success:
				tmp = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
				_,alpha = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
				b, g, r = cv2.split(image)
				rgba = [b,g,r, alpha]
				image = cv2.merge(rgba,4)
				cv2.imwrite(current_path + '/' +  file.replace('.mov', '-') + "{0}.png".format(str(counter)), image)
				counter += 1
				success,image = vidcap.read()